<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('site.home');
})->name('home');

Route::get('contato', function () {
    return view('site.contato');
})->name('contato');

Route::get('dicas-css', function () {
    return view('site.dicascss');
})->name('dicas-css');

Route::get('html', function () {
    return view('site.html');
})->name('html');

Route::get('javascript', function () {
    return view('site.javascript');
})->name('javascript');

Route::get('videoaulas', function () {
    return view('site.videoaulas');
})->name('videoaulas');
