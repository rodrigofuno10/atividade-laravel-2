@extends('layouts.site')

@section('titulo','videoaulas')


@section('conteudo')
<h2 id="aula">Video Aulas</h2>
<div class="container">
    <div class="row">
        <div class="col-5">
            <iframe width="350" height="200" src="https://www.youtube.com/embed/iZ1ucWosOww" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
        </div>
        <div class="col-6">
            <h2>Curso de HTML e CSS para iniciantes Aula </h2>

            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellendus quos maiores placeat amet
                consequatur libero dolores. Debitis illum voluptate veniam vitae, facere ad nostrum non, quis dolore
                earum repudiandae fugit!
            </p>
        </div>
    </div>
    <div class="row" >
        <div class="col-4">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/_i9DUJcn-mU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam fuga magnam,</p>
        </div>
        <div class="col-4">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/_i9DUJcn-mU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam fuga magnam,</p>
        </div>
        <div class="col-4">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/_i9DUJcn-mU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ullam fuga magnam,</p>
        </div>
    </div>
</div>
@endsection
