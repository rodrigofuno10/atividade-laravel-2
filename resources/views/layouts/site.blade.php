

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Web Mag - @yield('titulo')</title>
    <link rel="stylesheet" href="css/site.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="container " id="geral">
        <header id="cabecalho">
            <h1>
                <img src="img/logo.png" alt="Web Mag" class="img-fluid">
            </h1>
            <nav>
                <ul>
                    <li><a href="{{ route('home') }}">HOME</a></li>
                    <li><a href="{{ route('html') }}">HTML</a></li>
                    <li><a href="{{ route('javascript') }}">JAVASCRIPT</a></li>
                    <li><a href="{{ route('dicas-css') }}">CSS</a></li>
                    <li><a href="{{ route('videoaulas') }}">VÍDEO AULAS</a></li>
                    <li><a href="{{ route('contato') }}">CONTATO</a></li>
                </ul>
            </nav>
        </header>
        <section id="conteudo">
            @yield('conteudo')
        </section>

        <div id="borda">
        </div>
        <div id="rodape">
            <p>Todos os direitos são reservados</p>
        </div>

    </div>
</body>

</html>
